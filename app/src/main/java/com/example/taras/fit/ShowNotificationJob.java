package com.example.taras.fit;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationManagerCompat;

import com.evernote.android.job.Job;
import com.evernote.android.job.JobManager;
import com.evernote.android.job.JobRequest;
import com.example.taras.fit.Utils.PrefsUtils;
import com.example.taras.fit.Utils.TimeUtils;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.fitness.Fitness;
import com.google.android.gms.fitness.data.DataSet;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.data.Field;
import com.google.android.gms.fitness.result.DailyTotalResult;

import java.util.Random;
import java.util.concurrent.TimeUnit;

/**
 * Created by taras on 11/20/17.
 */

public class ShowNotificationJob extends Job {

    public static final String TAG = "show_notification_job_tag";

    @NonNull
    @Override
    protected Result onRunJob(Params params) {
        get3kStepsPerDayJob();
        return Result.SUCCESS;
    }

    public static void schedulePeriodic() {
        new JobRequest.Builder(ShowNotificationJob.TAG)
                .setPeriodic(TimeUnit.MINUTES.toMillis(15), TimeUnit.MINUTES.toMillis(5))
                .setRequiredNetworkType(JobRequest.NetworkType.CONNECTED)
                .setRequirementsEnforced(true)
                .setUpdateCurrent(true)
                .setRequiresBatteryNotLow(true)
                .build()
                .schedule();
    }

    private void get3kStepsPerDayJob() {
        GoogleApiClient client = new GoogleApiClient.Builder(getContext())
                .addApi(Fitness.HISTORY_API)
                .build();
        client.connect();

        PendingResult<DailyTotalResult> stepsResult = Fitness.HistoryApi
                .readDailyTotal(client, DataType.TYPE_STEP_COUNT_DELTA);
        stepsResult.setResultCallback(dailyTotalResult -> {
            if (dailyTotalResult.getStatus().isSuccess()) {
                DataSet totalSet = dailyTotalResult.getTotal();
                if (totalSet != null) {
                    long total = totalSet.isEmpty() ? 0 : totalSet.getDataPoints().get(0).getValue(Field.FIELD_STEPS).asInt();
                    initNotification(total);
                }
            }
        });
    }

    private void initNotification(long steps) {
        if (steps >= 3000) {
            showNotification();
        }
    }

    private void showNotification() {
        PrefsUtils.saveTimePrefs(TimeUtils.getEndOfDay(), getContext());

        createNotification();

        cancelJob();

        long afterThisTime = TimeUtils.getEndOfDay() - TimeUtils.getCurrentTime();

        createNewJobFotStartCurrentJob(afterThisTime);
    }

    private void createNotification() {
        PendingIntent pi = PendingIntent.getActivity(getContext(), 0,
                new Intent(getContext(), MainActivity.class), 0);
        Notification notification = new Notification.Builder(getContext())
                .setContentTitle("Android Job Demo")
                .setContentText("3k steps is completed!")
                .setAutoCancel(true)
                .setContentIntent(pi)
                .setSmallIcon(R.mipmap.ic_launcher)
                .build();

        NotificationManagerCompat.from(getContext()).notify(new Random().nextInt(), notification);
    }

    private void cancelJob() {
        JobManager.instance().cancelAllForTag(TAG);
    }

    private void createNewJobFotStartCurrentJob(long endDay) {
        SecondJob.scheduleRunner(endDay);
    }

}
