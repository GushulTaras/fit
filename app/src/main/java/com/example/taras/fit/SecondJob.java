package com.example.taras.fit;

import android.support.annotation.NonNull;

import com.evernote.android.job.Job;
import com.evernote.android.job.JobRequest;

/**
 * Created by taras on 11/21/17.
 */

public class SecondJob extends Job {

    public static final String TAG = "second_job_tag";

    @NonNull
    @Override
    protected Result onRunJob(Params params) {
        ShowNotificationJob.schedulePeriodic();
        return Result.SUCCESS;
    }

    public static void scheduleRunner(long time) {
        new JobRequest.Builder(SecondJob.TAG)
                .setExact(time)
                .build()
                .schedule();
    }

}
