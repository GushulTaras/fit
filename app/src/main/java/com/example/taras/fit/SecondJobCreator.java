package com.example.taras.fit;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.evernote.android.job.Job;
import com.evernote.android.job.JobCreator;

/**
 * Created by taras on 11/21/17.
 */

public class SecondJobCreator implements JobCreator {

    @Nullable
    @Override
    public Job create(@NonNull String tag) {
        switch (tag) {
            case SecondJob.TAG:
                return new SecondJob();
            default:
                return null;
        }
    }
}
