package com.example.taras.fit.Utils;

import android.content.Context;

/**
 * Created by taras on 11/21/17.
 */

public class PrefsUtils {

    public static void saveTimePrefs(long time, Context context) {
        context.getSharedPreferences("timePrefs", Context.MODE_PRIVATE)
                .edit()
                .putLong("time", time)
                .apply();
    }

    public static long getTimePrefs(Context context) {
        return context.getSharedPreferences("timePrefs", Context.MODE_PRIVATE)
                .getLong("time", 0);
    }
}
