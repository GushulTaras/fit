package com.example.taras.fit;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.example.taras.fit.Utils.PrefsUtils;
import com.example.taras.fit.Utils.TimeUtils;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.fitness.Fitness;
import com.google.android.gms.fitness.FitnessOptions;
import com.google.android.gms.fitness.data.Bucket;
import com.google.android.gms.fitness.data.DataPoint;
import com.google.android.gms.fitness.data.DataSet;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.data.Field;
import com.google.android.gms.fitness.data.Value;
import com.google.android.gms.fitness.request.DataReadRequest;
import com.google.android.gms.fitness.result.DailyTotalResult;
import com.google.android.gms.fitness.result.DataReadResult;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class MainActivity extends AppCompatActivity {
    private static final int GOOGLE_FIT_PERMISSIONS_REQUEST_CODE = 1;
    private static final String TAG = MainActivity.class.getSimpleName();
    private TextView steps;
    private TextView calories;
    private TextView sleep;
    private TextView workout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        initTextViews();
//        createFitness();

        if (TimeUtils.getCurrentTime() > PrefsUtils.getTimePrefs(this)) {
            createFitnessJob();
        }

    }

    private void createFitnessJob() {
        FitnessOptions fitnessOptions = FitnessOptions.builder()
                .addDataType(DataType.TYPE_STEP_COUNT_DELTA, FitnessOptions.ACCESS_READ)
                .build();

        if (!GoogleSignIn.hasPermissions(GoogleSignIn.getLastSignedInAccount(this), fitnessOptions)) {
            GoogleSignIn.requestPermissions(
                    this,
                    GOOGLE_FIT_PERMISSIONS_REQUEST_CODE,
                    GoogleSignIn.getLastSignedInAccount(this),
                    fitnessOptions);
        } else {
            ShowNotificationJob.schedulePeriodic();
        }
    }

    private void initTextViews() {
        steps = findViewById(R.id.steps);
        calories = findViewById(R.id.calories);
        sleep = findViewById(R.id.sleep);
        workout = findViewById(R.id.workout);
    }

    private void createFitness() {
        FitnessOptions fitnessOptions = FitnessOptions.builder()
                .addDataType(DataType.TYPE_STEP_COUNT_DELTA, FitnessOptions.ACCESS_READ)
//                .addDataType(DataType.AGGREGATE_STEP_COUNT_DELTA, FitnessOptions.ACCESS_READ)
                .addDataType(DataType.TYPE_CALORIES_EXPENDED, FitnessOptions.ACCESS_READ)
                .addDataType(DataType.TYPE_ACTIVITY_SEGMENT, FitnessOptions.ACCESS_READ)
                .addDataType(DataType.TYPE_WORKOUT_EXERCISE, FitnessOptions.ACCESS_READ)
                .build();

        if (!GoogleSignIn.hasPermissions(GoogleSignIn.getLastSignedInAccount(this), fitnessOptions)) {
            GoogleSignIn.requestPermissions(
                    this,
                    GOOGLE_FIT_PERMISSIONS_REQUEST_CODE,
                    GoogleSignIn.getLastSignedInAccount(this),
                    fitnessOptions);
        } else {
            getData();
//            getStepsAggregate();
        }
    }

    private void getData() {
        GoogleApiClient client = new GoogleApiClient.Builder(this)
                .addApi(Fitness.HISTORY_API)
                .build();
        client.connect();

        PendingResult<DailyTotalResult> stepsResult = Fitness.HistoryApi
                .readDailyTotal(client, DataType.TYPE_STEP_COUNT_DELTA);
        stepsResult.setResultCallback(dailyTotalResult -> {
            if (dailyTotalResult.getStatus().isSuccess()) {
                DataSet totalSet = dailyTotalResult.getTotal();
                if (totalSet != null) {
                    long total = totalSet.isEmpty() ? 0 : totalSet.getDataPoints().get(0).getValue(Field.FIELD_STEPS).asInt();
                    String stepsCount = "Steps: " + total;
                    steps.setText(stepsCount);
                    Log.i(TAG, "count of steps: " + total);
                }
            }
        });

//        PendingResult<DailyTotalResult> caloriesResult = Fitness.HistoryApi
//                .readDailyTotal(client, DataType.TYPE_CALORIES_EXPENDED);
//        caloriesResult.setResultCallback(dailyTotalResult -> {
//
//            if (dailyTotalResult.getStatus().isSuccess()) {
//                DataSet totalSet = dailyTotalResult.getTotal();
//                if (totalSet != null) {
//                    float total = totalSet.isEmpty() ? 0 : totalSet.getDataPoints().get(0).getValue(Field.FIELD_CALORIES).asFloat();
//                    String caloriesCount = "Calories: " + total;
//                    calories.setText(caloriesCount);
//                    Log.i(TAG, "count of calories: " + total);
//                }
//            }
//        });

        getActivitiesResult(client);

        getCaloriesAggregate(client);
    }

    private void getActivitiesResult(GoogleApiClient client) {
        PendingResult<DailyTotalResult> activityResult = Fitness.HistoryApi
                .readDailyTotal(client, DataType.TYPE_ACTIVITY_SEGMENT);
        activityResult.setResultCallback(dailyTotalResult -> {

            if (dailyTotalResult.getStatus().isSuccess()) {
                DataSet totalSet = dailyTotalResult.getTotal();
                getTimeOfActivity(totalSet);
            }
        });
    }

    private void getTimeOfActivity(DataSet dataSet) {
        for (DataPoint dataPoint : dataSet.getDataPoints()) {
            for (Field field : dataPoint.getDataType().getFields()) {
                Value val = dataPoint.getValue(field);
                if (field.getName().trim().toLowerCase().equals("activity")) {
                    long start = dataPoint.getStartTime(TimeUnit.SECONDS);
                    long end = dataPoint.getEndTime(TimeUnit.SECONDS);
                    long result = end - start;
                    Log.i(TAG, String.valueOf(result) + " seconds");

                    Log.i(TAG, "val: " + val.asInt());
                    String string = val.asActivity();
                    Log.i(TAG, "string: " + string);
                }
            }
        }
    }

    private void getCaloriesAggregate(GoogleApiClient client) {
        Calendar cal = Calendar.getInstance();
        Date now = new Date();
        cal.setTime(now);
        long endTime = cal.getTimeInMillis();
        cal.add(Calendar.DAY_OF_YEAR, -1);
        long startTime = cal.getTimeInMillis();

        DataReadRequest readRequest = new DataReadRequest.Builder()
                .aggregate(DataType.TYPE_CALORIES_EXPENDED, DataType.AGGREGATE_CALORIES_EXPENDED)
                .bucketByActivityType(1, TimeUnit.SECONDS)
                .setTimeRange(startTime, endTime, TimeUnit.MILLISECONDS)
                .build();

        Fitness.HistoryApi.readData(client, readRequest).setResultCallback(this::getCalories);
    }

    private void getCalories(DataReadResult dataReadResult) {
        if (dataReadResult.getBuckets().size() > 0) {
            for (Bucket bucket : dataReadResult.getBuckets()) {
                List<DataSet> dataSets = bucket.getDataSets();
                Log.i(TAG, "bucket: " + bucket.getActivity());
                for (DataSet dataSet : dataSets) {
                    processCalories(dataSet);
                }
            }
        }
    }

    private void processCalories(DataSet dataSet) {
        for (DataPoint dp : dataSet.getDataPoints()) {
            for (Field field : dp.getDataType().getFields()) {
                String fieldName = field.getName();
                Log.i(TAG, "\tFieldCal: " + fieldName + " ValueCal: " + dp.getValue(field));
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.i(TAG, "onActivityResult()");
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == GOOGLE_FIT_PERMISSIONS_REQUEST_CODE) {
//                getData();
                ShowNotificationJob.schedulePeriodic();
//                getStepsAggregate();
            }
        }
    }

    private void getStepsAggregate() {
        GoogleApiClient client = new GoogleApiClient.Builder(this)
                .addApi(Fitness.HISTORY_API)
                .build();
        client.connect();

        Calendar cal = Calendar.getInstance();
        Date now = new Date();
        cal.setTime(now);
        long endTime = cal.getTimeInMillis();
        cal.add(Calendar.WEEK_OF_YEAR, -1);
        long startTime = cal.getTimeInMillis();

        DataReadRequest readRequest = new DataReadRequest.Builder()
                .aggregate(DataType.TYPE_STEP_COUNT_DELTA, DataType.AGGREGATE_STEP_COUNT_DELTA)
                .bucketByTime(1, TimeUnit.DAYS)
                .setTimeRange(startTime, endTime, TimeUnit.MILLISECONDS)
                .build();

        Fitness.HistoryApi.readData(client, readRequest).setResultCallback(this::getResultSteps);
    }

    private void getResultSteps(DataReadResult dataReadResult) {
        if (dataReadResult.getBuckets().size() > 0) {
            for (Bucket bucket : dataReadResult.getBuckets()) {
                List<DataSet> dataSets = bucket.getDataSets();
                for (DataSet dataSet : dataSets) {
                    processDataSet(dataSet);
                }
            }
        }
    }

    private void processDataSet(DataSet dataSet) {
        for (DataPoint dp : dataSet.getDataPoints()) {
            for (Field field : dp.getDataType().getFields()) {
                String fieldName = field.getName();
                Log.i("TAG", "\tField: " + fieldName + " Value: " + dp.getValue(field));
            }
        }
    }

}
