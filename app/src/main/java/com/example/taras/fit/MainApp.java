package com.example.taras.fit;

import android.app.Application;

import com.evernote.android.job.JobManager;

/**
 * Created by taras on 11/20/17.
 */

public class MainApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        JobManager.create(this).addJobCreator(new DemoJobCreator());
        JobManager.create(this).addJobCreator(new SecondJobCreator());
    }
}
